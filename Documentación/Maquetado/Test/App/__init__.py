# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 23:58:16 2018

@author: intel 2015
"""
import os

import flask
from flask import Flask, session, flash, jsonify
from flask import request
from flask import render_template
from App.services import usuario_service

app = Flask(__name__)


# LOGICA DE CONTROLADORES


# LOGIN CONTROLLER
@app.route('/')
def login():
    if 'logged_in' in session and session['logged_in']:
        result = render_template('inicio/inicio.html')
    else:
        result = render_template('login/login.html')
    return result


@app.route('/send_login', methods=['POST', 'GET'])
def send_login():
    if request.method == 'POST':
        email = request.form['email']
        contrasenia = request.form['contrasenia']
        if usuario_service.autenticar(email, contrasenia):
            session['logged_in'] = True
            session['email_usuario'] = email
            result = render_template('inicio/inicio.html')
        else:
            session['logged_in'] = False
            result = render_template('login/login.html')
    return result


@app.route('/signin')
def signin():
    result = render_template('login/signin.html')
    return result


@app.route('/send_signin', methods=['POST', 'GET'])
def send_signin():
    try:
        if request.method == 'POST':
            nombre = request.form['nombre']
            apellido = request.form['apellido']
            email = request.form['email']
            contrasenia = request.form['contrasenia']
            respuesta = usuario_service.registrar(nombre, apellido, email, contrasenia)
            if respuesta.codigo == 200:
                flash(respuesta.mensaje)
                result = render_template('login/login.html')
            else:
                flash(respuesta.mensaje)
                result = render_template('login/signin.html')
    except NameError:
        flash("Ha ocurrido un error interno en la página. Vuelva a intentarlo.")
        result = render_template('login/signin.html')
    return result


@app.route('/logout', methods=['POST', 'GET'])
def logout():
    if request.method == 'POST':
        session.pop('logged_in', None)
        session.pop('email_usuario', None)
    flash('Has salido.')
    return render_template('login/login.html')


@app.route('/admin', methods=['POST', 'GET'])
def admin():
    usuarios = usuario_service.traer_usuarios()
    return render_template('admin/inicio.html', usuarios=usuarios)


@app.route('/_autorizar_usuario')
def _autorizar_usuario():
    try:
        _id = request.args.get('email', '', type=str)
        autorizado = request.args.get('autorizado', True, type=bool)
        respuesta = usuario_service.autorizar_usuario(_id, autorizado)
        return jsonify(result = respuesta)
    except NameError as error:
        return jsonify(result = error)



# FIN LOGIN CONTROLLER


# FIN LOGICA DE CONTROLADORES


if __name__ == "__main__":
    app.secret_key = os.urandom(12)  # TODO: ¿PARA QUE SE USA LA SECRET_KEY?
    app.run(debug=True, host='localhost', port=4000)
