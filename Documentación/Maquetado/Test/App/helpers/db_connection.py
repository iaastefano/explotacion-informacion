from pymongo import MongoClient
from pymongo import database
from pymongo import collection
host = 'localhost'
puerto = 27017
db_name = 'explotacion-informacion'
print("conectandose a la base de datos...")
print("host: " + host)
print("puerto: " + str(puerto))
print("base de datos: " + db_name)
client: MongoClient
db: database
usuarios: collection
try:
    client = MongoClient(host, puerto)
    db = client[db_name]
    print("conexion exitosa.")
    usuarios = db['usuarios']
except NameError as e:
    print("error al conectarse a la base de datos. " + e)

