class respuesta(object):
    def __init__(self, estado, codigo, mensaje):
        self.estado: str = estado
        self.codigo: int = codigo
        self.mensaje: str = mensaje

    def to_string(self):
        return self.estado + " | " + str(self.codigo) + " | " + self.mensaje