from App.helpers import db_connection as db
from App.helpers.respuesta import respuesta
import datetime
import logging
from App.model import Model

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    filename='/temp/app.log',
                    filemode='w')


def autenticar(mail: str, contrasenia: str):
    logging.debug("INICIO | autenticar | mail: " + mail + ", contrasenia: " + contrasenia)
    try:
        usuarios = db.usuarios.find({'email': mail, 'contrasenia': contrasenia})
        if usuarios.count() == 1:
            result = respuesta("Exito", 200, "Bienvenido al sistema!")
            logging.debug("FIN | autenticar | mail: " + mail + ", contrasenia: " + contrasenia + " return: " + result.to_string())
        else:
            result = respuesta("Fallo", 300, "Usuario o contrasenia incorrecto.")
            logging.warning("FIN | autenticar | mail: " + mail + ", contrasenia: " + contrasenia + " return: " + result.to_string())
    except NameError as error:
        result = respuesta("Error", 100, "No se pudo autenticar el usuario.")
        logging.error("ERROR | autenticar | mail: " + mail + ", contrasenia: " + contrasenia + " return: " + result.to_string() +
                      " error: " + error)
    return result

#vanesa fundacion bioabordaje del autismo streaming evento


def registrar(nombre: str, apellido: str, mail: str, contrasenia: str):
    try:
        usuarios = db.usuarios.find({'email': mail})
        if usuarios.count() > 0:
            result = respuesta("Fallo", 300, "Ya existe un usuario con este email.")
        else:
            db.usuarios.insert_one({'email': mail, 'contrasenia': contrasenia, 'nombre': nombre, 'apellido': apellido,
                                    'fecha_ingreso' : datetime.datetime.now()})
            result = respuesta("Exito", 200, "Registro exitoso!")
    except NameError as error:
        result = respuesta("Error", 100, "Ha ocurrido un error al registrar el usuario.")

    return result


def traer_usuario_por_id(_id: str):
    return Model(db.usuarios.find_one({'id': id}))


def traer_usuario(email: str):
    return Model(db.usuarios.find_one({'email': email}))


def traer_usuarios():
    usuarios = []
    for usuario in db.usuarios.find():
        usuarios.append(Model(usuario))
    return usuarios


def modificar_usuario(usuario):
    db.usuarios.update(usuario)


def autorizar_usuario(_id: str, autorizado: bool):
    try:
        usuario = traer_usuario_por_id(_id)
        if usuario.autorizado == autorizado:
            if usuario.autorizado:
                mensaje = "El usuario ya estaba autorizado anteriormente."
            else:
                mensaje = "El usuario ya no estaba autorizado anteriormente."
            result = respuesta("Fallo", 300, mensaje)
        else:
            usuario.autorizado = autorizado

            if usuario.autorizado:
                mensaje = "El usuario ha sido autorizado con exito!"
            else:
                mensaje = "El usuario ha sido desautorizado con exito!"
            result = respuesta("Exito", 200, mensaje)
    except NameError as error:
        if autorizado:
            mensaje = "Ha ocurrido un error al autorizar al usuario."
        else:
            mensaje = "Ha ocurrido un error al desautorizar al usuario."
        result = respuesta("Error", 100, mensaje)
    return result




