# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 23:58:16 2018

@author: intel 2015
"""

from Usuario import *
from flask import Flask
from flask import request
from flask import render_template

def ValidarUsuario(mail, contra):
    client = MongoClient('localhost',27017)
    db = client['local']
    collection_user = db['usuarios']
    res = collection_user.find({'email': mail , 'contrasenia': contra, 'activo': True})
    if ( res.count() == 1):
        envio = True
    else:
        envio = False
    return envio

def usuarioExistente(mail):
    client = MongoClient('localhost',27017)
    db = client['local']
    collection_user = db['usuarios']
    res = collection_user.find({'email': mail})
    if ( res.count() == 1):
        envio = True
    else:
        envio = False
    return envio


def recorrerTabla(table_name):
    client = MongoClient('localhost',27017)
    db = client['local']
    collection_user = db[table_name]
    res = collection_user.find()
    for document in res:
        pprint(document)

def traerUltimoId(table_name):
    client = MongoClient('localhost',27017)
    db = client['local']
    collection_user = db[table_name]
    res = collection_user.find()
    i = 0
    for document in res:
        i+=1
    return i

def agregarUsuario( idUsuario, nombre, apellido, email, contrasenia, lstProyecto, activo, fecha_creacion  ):
    user = Usuario( idUsuario, nombre, apellido, email, contrasenia, lstProyecto, activo, fecha_creacion)
    user.toDBCollecion()
    envio1 = ' Usuario creado'
    return envio1

def traerIdUsuario(mail):
    client = MongoClient('localhost',27017)
    db = client['local']
    collection_user = db['usuarios']
    res = collection_user.find({'email': mail})
    for document in res:
        if(res):
            id = document['idUsuario']
    return id
    # a = db.usuarios.find({email:mail}).pretty()

def conexiondb( idUsuario, nombre, apellido, email, contrasenia, lstProyecto, activo, fecha_creacion):
     if ( usuarioExistente(email) == 1 ):
         envio = 'usuario Existente'
     else:
         envio = agregarUsuario( idUsuario, nombre, apellido, email, contrasenia, lstProyecto, activo, fecha_creacion)
     return envio;



app = Flask(__name__)

@app.route('/')
def login():
    return render_template('login.html')


@app.route('/verificacionLogin',methods=['POST','GET'])
def resultadoLogin():
    if request.method == 'POST':
        email=request.form['email']
        password=request.form['password']
        verificar = ValidarUsuario(email,password)
        user = traerIdUsuario(email)
        if( verificar == True):
            pagina = 'index.html'
        else:
            pagina = 'registro.html'
    return render_template(pagina,user=user);


@app.route('/registro')
def registro():
    return render_template('registro.html')

@app.route('/envioRegistro',methods = ['POST', 'GET'])
def resultadoRegistro():

    lstProyectoVacio = [0, 0, 0]
    if request.method == 'POST':
      # lo que tenes que hacer es crear un objeto Usuario y guardar estos datos que recibo del formulario registro
      name=request.form['name']
      apellido=request.form['apellido']
      email=request.form['email']
      password=request.form['password']
      repetPassword=request.form['repetPassword']
      if ( password == repetPassword ):
          id = traerUltimoId('usuarios') + 1
          creado = True
          name = conexiondb(id,name,apellido,email,password,lstProyectoVacio,creado, datetime.datetime.now())
      else:
          name = ' has puesto mal la contraseña'
      return render_template('resultado.html',name = name)


@app.route('/crearProyecto',methods = ['POST', 'GET'])
def generarDataSetVista():
    if request.method == 'POST':
        nombreProyecto=request.form['nombreProyecto'];
        descripcionProyecto=request.form['descripcionProyecto'];
        fechaDeCreacion=datetime.datetime.now();
        idUsuario=request.form['idUsuario']
        
    return render_template('definirDataset.html',nombreProyecto=nombreProyecto,descripcionProyecto=descripcionProyecto,fechaDeCreacion=fechaDeCreacion,idUsuario=idUsuario)



@app.route('/generarDatasetVista',methods = ['POST', 'GET'])
def generarProyecto():
    if request.method == 'POST':
       idUsuario=request.form['idUsuario']
    return render_template('generarDataset.html',idUsuario=idUsuario)



@app.route('/index')
def index():
    return render_template('index.html')
app.run()
