import datetime;
from pymongo import MongoClient
from pymongo import ReturnDocument
from pprint import pprint
class Usuario:
    
    def __init__(self, idUsuario, nombre, apellido, email, contrasenia, lstProyecto, activo, fecha_creacion):
        self._idUsuario = idUsuario
        self._nombre = nombre
        self._apellido = apellido
        self._email = email
        self._contrasenia = contrasenia
        self._lstProyecto = lstProyecto
        self._activo = activo
        self._fecha_creacion = fecha_creacion

    def get_id_usuario(self):
        return self.__idUsuario


    def get_nombre(self):
        return self.__nombre


    def get_apellido(self):
        return self.__apellido


    def get_email(self):
        return self.__email


    def get_contrasenia(self):
        return self.__contrasenia


    def get_lst_proyectos(self):
        return self.__lstProyecto


    def get_activo(self):
        return self.__activo


    def get_fecha_creacion(self):
        return self.__fecha_creacion


    def set_id_usuario(self, value):
        self.__idUsuario = value


    def set_nombre(self, value):
        self.__nombre = value


    def set_apellido(self, value):
        self.__apellido = value


    def set_email(self, value):
        self.__email = value


    def set_contrasenia(self, value):
        self.__contrasenia = value


    def set_lst_proyectos(self, value):
        self.__lstProyecto = value


    def set_activo(self, value):
        self.__activo = value


    def set_fecha_creacion(self, value):
        self.__fecha_creacion = value


    def del_id_usuario(self):
        del self.__idUsuario


    def del_nombre(self):
        del self.__nombre


    def del_apellido(self):
        del self.__apellido


    def del_email(self):
        del self.__email


    def del_contrasenia(self):
        del self.__contrasenia


    def del_lst_proyectos(self):
        del self.__lstProyecto


    def del_activo(self):
        del self.__activo


    def del_fecha_creacion(self):
        del self.__fecha_creacion

    def toDBCollecion(self):
        client = MongoClient('localhost',27017)
        db = client['local']
        conectando = db['usuarios']
        conectando.insert_one({
        "idUsuario": self._idUsuario,
        "nombre": self._nombre,
        "apellido": self._apellido,
        "email": self._email,
        "contrasenia": self._contrasenia,
        "lstProyectos": self._lstProyecto,
        "activo": self._activo,
        "fecha_ingreso": self._fecha_creacion
            })


    idUsuario = property(get_id_usuario, set_id_usuario, del_id_usuario, "idUsuario's docstring")
    nombre = property(get_nombre, set_nombre, del_nombre, "nombre's docstring")
    apellido = property(get_apellido, set_apellido, del_apellido, "apellido's docstring")
    email = property(get_email, set_email, del_email, "email's docstring")
    contrasenia = property(get_contrasenia, set_contrasenia, del_contrasenia, "contrasenia's docstring")
    lstProyecto = property(get_lst_proyectos, set_lst_proyectos, del_lst_proyectos, "lstProyectos's docstring")
    activo = property(get_activo, set_activo, del_activo, "activo's docstring")
    fecha_creacion = property(get_fecha_creacion, set_fecha_creacion, del_fecha_creacion, "fecha_creacion's docstring")
