from pymongo import MongoClient
from pymongo import ReturnDocument
from pprint import pprint
import datetime
class Usuario:
    def __init__(self,iduser,name,surname,email,contra):
        self.client = MongoClient('Localhost',27017)
        self.db = self.client['local']
        self.collection_User = self.db['usuarios']
        self.iduser =  iduser
        self.name = name
        self.surname = surname
        self.email = email
        self.contra = contra
    def toDBCollecion(self):
        self.collection_User.insert_one({
            "id": self.iduser,
            "nombre": self.name,
            "apellido": self.surname,
            "email": self.email,
            "contraseņa": self.contra,
            "activo": "true",
            "fecha_ingreso": datetime.datetime.now()
        })
def recorrerTabla(table_name):
    client = MongoClient('localhost',27017)
    db = client['local']
    collection_user = db[table_name]
    res = collection_user.find()
    for document in res:
        pprint(document)
        
        
if __name__ == "__main__":
    #User1 = Usuario("7","Esteban","Rodriguez","rodriguezEsteban@gmail.com","asddas123")
    #User1.toDBCollecion()
    recorrerTabla('Algoritmos')
    