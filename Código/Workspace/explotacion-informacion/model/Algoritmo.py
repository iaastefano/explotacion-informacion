
class AlgoritmoAplicado:
    def __init__(self, idAlgoritmoAplicado, lstResultados, algoritmo, dataset,lstVariables, algoritmoPadre):
        self._idAlgoritmoAplicado = idAlgoritmoAplicado
        self._lstResultados = lstResultados
        self._algoritmo = algoritmo
        self._dataset = dataset
        self._lstVariables = lstVariables
        self._algoritmoPadre = algoritmoPadre

    def get_id_algoritmo_aplicado(self):
        return self.__idAlgoritmoAplicado


    def get_lst_resultados(self):
        return self.__lstResultados


    def get_algoritmo(self):
        return self.__algoritmo


    def get_dataset(self):
        return self.__dataset


    def get_lst_variables(self):
        return self.__lstVariables


    def get_algoritmo_padre(self):
        return self.__algoritmoPadre


    def set_id_algoritmo_aplicado(self, value):
        self.__idAlgoritmoAplicado = value


    def set_lst_resultados(self, value):
        self.__lstResultados = value


    def set_algoritmo(self, value):
        self.__algoritmo = value


    def set_dataset(self, value):
        self.__dataset = value


    def set_lst_variables(self, value):
        self.__lstVariables = value


    def set_algoritmo_padre(self, value):
        self.__algoritmoPadre = value


    def del_id_algoritmo_aplicado(self):
        del self.__idAlgoritmoAplicado


    def del_lst_resultados(self):
        del self.__lstResultados


    def del_algoritmo(self):
        del self.__algoritmo


    def del_dataset(self):
        del self.__dataset


    def del_lst_variables(self):
        del self.__lstVariables


    def del_algoritmo_padre(self):
        del self.__algoritmoPadre

    idAlgoritmoAplicado = property(get_id_algoritmo_aplicado, set_id_algoritmo_aplicado, del_id_algoritmo_aplicado, "idAlgoritmoAplicado's docstring")
    lstResultados = property(get_lst_resultados, set_lst_resultados, del_lst_resultados, "lstResultados's docstring")
    algoritmo = property(get_algoritmo, set_algoritmo, del_algoritmo, "algoritmo's docstring")
    dataset = property(get_dataset, set_dataset, del_dataset, "dataset's docstring")
    lstVariables = property(get_lst_variables, set_lst_variables, del_lst_variables, "lstVariables's docstring")
    algoritmoPadre = property(get_algoritmo_padre, set_algoritmo_padre, del_algoritmo_padre, "algoritmoPadre's docstring")
        
    