
class Resultado:
    def __init__(self, idResultado, resultado):
        self._idResultado = idResultado
        self._resultado = resultado    

    def get_id_resultado(self):
        return self.__idResultado


    def get_resultado(self):
        return self.__resultado


    def set_id_resultado(self, value):
        self.__idResultado = value


    def set_resultado(self, value):
        self.__resultado = value


    def del_id_resultado(self):
        del self.__idResultado


    def del_resultado(self):
        del self.__resultado

    idResultado = property(get_id_resultado, set_id_resultado, del_id_resultado, "idResultado's docstring")
    resultado = property(get_resultado, set_resultado, del_resultado, "resultado's docstring")
        