

class Regla:

    def __init__(self, idRegla, nombre, lstCondiciones, totalRegistros):
        self._idRegla = idRegla
        self._nombre = nombre
        self._lstCondiciones = lstCondiciones
        self._totalRegistros = totalRegistros

    def get_id_regla(self):
        return self.__idRegla


    def get_nombre(self):
        return self.__nombre


    def get_lst_condiciones(self):
        return self.__lstCondiciones


    def get_total_registros(self):
        return self.__totalRegistros


    def set_id_regla(self, value):
        self.__idRegla = value


    def set_nombre(self, value):
        self.__nombre = value


    def set_lst_condiciones(self, value):
        self.__lstCondiciones = value


    def set_total_registros(self, value):
        self.__totalRegistros = value


    def del_id_regla(self):
        del self.__idRegla


    def del_nombre(self):
        del self.__nombre


    def del_lst_condiciones(self):
        del self.__lstCondiciones


    def del_total_registros(self):
        del self.__totalRegistros

    idRegla = property(get_id_regla, set_id_regla, del_id_regla, "idRegla's docstring")
    nombre = property(get_nombre, set_nombre, del_nombre, "nombre's docstring")
    lstCondiciones = property(get_lst_condiciones, set_lst_condiciones, del_lst_condiciones, "lstCondiciones's docstring")
    totalRegistros = property(get_total_registros, set_total_registros, del_total_registros, "totalRegistros's docstring")
        
    