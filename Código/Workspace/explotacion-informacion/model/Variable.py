
class Variable:
    def __init__(self, idVariable, nombre, tipoDato, dominio, valores):
        self._idVariable = idVariable
        self._nombre = nombre
        self._tipoDato = tipoDato
        self._dominio = dominio
        self._valores = valores

    def get_id_variable(self):
        return self.__idVariable


    def get_nombre(self):
        return self.__nombre


    def get_tipo_dato(self):
        return self.__tipoDato


    def get_dominio(self):
        return self.__dominio


    def get_valores(self):
        return self.__valores


    def set_id_variable(self, value):
        self.__idVariable = value


    def set_nombre(self, value):
        self.__nombre = value


    def set_tipo_dato(self, value):
        self.__tipoDato = value


    def set_dominio(self, value):
        self.__dominio = value


    def set_valores(self, value):
        self.__valores = value


    def del_id_variable(self):
        del self.__idVariable


    def del_nombre(self):
        del self.__nombre


    def del_tipo_dato(self):
        del self.__tipoDato


    def del_dominio(self):
        del self.__dominio


    def del_valores(self):
        del self.__valores

    idVariable = property(get_id_variable, set_id_variable, del_id_variable, "idVariable's docstring")
    nombre = property(get_nombre, set_nombre, del_nombre, "nombre's docstring")
    tipoDato = property(get_tipo_dato, set_tipo_dato, del_tipo_dato, "tipoDato's docstring")
    dominio = property(get_dominio, set_dominio, del_dominio, "dominio's docstring")
    valores = property(get_valores, set_valores, del_valores, "valores's docstring")
        
    
                 


        