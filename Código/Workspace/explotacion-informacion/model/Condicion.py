
class Condicion:
    def __init__(self, idCondicion, variable, rango, cantRegistros, porcentajeRegistros):
            self._idCondicion = idCondicion
            self._variable = variable
            self._rango = rango
            self._cantRegistros = cantRegistros
            self._porcentajeRegistros = porcentajeRegistros

    def get_id_condicion(self):
        return self.__idCondicion


    def get_variable(self):
        return self.__variable


    def get_rango(self):
        return self.__rango


    def get_cant_registros(self):
        return self.__cantRegistros


    def get_porcentaje_registros(self):
        return self.__porcentajeRegistros


    def set_id_condicion(self, value):
        self.__idCondicion = value


    def set_variable(self, value):
        self.__variable = value


    def set_rango(self, value):
        self.__rango = value


    def set_cant_registros(self, value):
        self.__cantRegistros = value


    def set_porcentaje_registros(self, value):
        self.__porcentajeRegistros = value


    def del_id_condicion(self):
        del self.__idCondicion


    def del_variable(self):
        del self.__variable


    def del_rango(self):
        del self.__rango


    def del_cant_registros(self):
        del self.__cantRegistros


    def del_porcentaje_registros(self):
        del self.__porcentajeRegistros

    idCondicion = property(get_id_condicion, set_id_condicion, del_id_condicion, "idCondicion's docstring")
    variable = property(get_variable, set_variable, del_variable, "variable's docstring")
    rango = property(get_rango, set_rango, del_rango, "rango's docstring")
    cantRegistros = property(get_cant_registros, set_cant_registros, del_cant_registros, "cantRegistros's docstring")
    porcentajeRegistros = property(get_porcentaje_registros, set_porcentaje_registros, del_porcentaje_registros, "porcentajeRegistros's docstring")
