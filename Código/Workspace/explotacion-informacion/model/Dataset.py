
class Dataset:
    def __init__(self, idDataset, nombre, archivo_importado, formato, importado, fecha_ingreso, lstReglas, lstAlgoritmosAplicados, lstVariables):
        self._idDataset = idDataset
        self._nombre = nombre
        self._archivo_importado = archivo_importado
        self._formato = formato
        self._importado = importado
        self._fecha_ingreso = fecha_ingreso
        self._lstReglas = lstReglas
        self._lstAlgoritmosAplicados = lstAlgoritmosAplicados
        self._lstVariables = lstVariables

    def get_id_dataset(self):
        return self.__idDataset


    def get_nombre(self):
        return self.__nombre


    def get_archivo_importado(self):
        return self.__archivo_importado


    def get_formato(self):
        return self.__formato


    def get_importado(self):
        return self.__importado


    def get_fecha_ingreso(self):
        return self.__fecha_ingreso


    def get_lst_reglas(self):
        return self.__lstReglas


    def get_lst_algoritmos_aplicados(self):
        return self.__lstAlgoritmosAplicados


    def get_lst_variables(self):
        return self.__lstVariables


    def set_id_dataset(self, value):
        self.__idDataset = value


    def set_nombre(self, value):
        self.__nombre = value


    def set_archivo_importado(self, value):
        self.__archivo_importado = value


    def set_formato(self, value):
        self.__formato = value


    def set_importado(self, value):
        self.__importado = value


    def set_fecha_ingreso(self, value):
        self.__fecha_ingreso = value


    def set_lst_reglas(self, value):
        self.__lstReglas = value


    def set_lst_algoritmos_aplicados(self, value):
        self.__lstAlgoritmosAplicados = value


    def set_lst_variables(self, value):
        self.__lstVariables = value


    def del_id_dataset(self):
        del self.__idDataset


    def del_nombre(self):
        del self.__nombre


    def del_archivo_importado(self):
        del self.__archivo_importado


    def del_formato(self):
        del self.__formato


    def del_importado(self):
        del self.__importado


    def del_fecha_ingreso(self):
        del self.__fecha_ingreso


    def del_lst_reglas(self):
        del self.__lstReglas


    def del_lst_algoritmos_aplicados(self):
        del self.__lstAlgoritmosAplicados


    def del_lst_variables(self):
        del self.__lstVariables

    idDataset = property(get_id_dataset, set_id_dataset, del_id_dataset, "idDataset's docstring")
    nombre = property(get_nombre, set_nombre, del_nombre, "nombre's docstring")
    archivo_importado = property(get_archivo_importado, set_archivo_importado, del_archivo_importado, "archivo_importado's docstring")
    formato = property(get_formato, set_formato, del_formato, "formato's docstring")
    importado = property(get_importado, set_importado, del_importado, "importado's docstring")
    fecha_ingreso = property(get_fecha_ingreso, set_fecha_ingreso, del_fecha_ingreso, "fecha_ingreso's docstring")
    lstReglas = property(get_lst_reglas, set_lst_reglas, del_lst_reglas, "lstReglas's docstring")
    lstAlgoritmosAplicados = property(get_lst_algoritmos_aplicados, set_lst_algoritmos_aplicados, del_lst_algoritmos_aplicados, "lstAlgoritmosAplicados's docstring")
    lstVariables = property(get_lst_variables, set_lst_variables, del_lst_variables, "lstVariables's docstring")
    
    