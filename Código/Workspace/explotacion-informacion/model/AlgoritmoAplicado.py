
class Algoritmo:
    def __init__(self, idAlgoritmo, nombre, descripcion, precondiciones, inputs, targets, poscondiciones):
        self._idAlgoritmo = idAlgoritmo
        self._nombre = nombre
        self._descripcion = descripcion
        self._precondiciones = precondiciones
        self._inputs = inputs
        self._targets = targets
        self._poscondiciones = poscondiciones

    def get_id_algoritmo(self):
        return self.__idAlgoritmo


    def get_nombre(self):
        return self.__nombre


    def get_descripcion(self):
        return self.__descripcion


    def get_precondiciones(self):
        return self.__precondiciones


    def get_inputs(self):
        return self.__inputs


    def get_targets(self):
        return self.__targets


    def get_poscondiciones(self):
        return self.__poscondiciones


    def set_id_algoritmo(self, value):
        self.__idAlgoritmo = value


    def set_nombre(self, value):
        self.__nombre = value


    def set_descripcion(self, value):
        self.__descripcion = value


    def set_precondiciones(self, value):
        self.__precondiciones = value


    def set_inputs(self, value):
        self.__inputs = value


    def set_targets(self, value):
        self.__targets = value


    def set_poscondiciones(self, value):
        self.__poscondiciones = value


    def del_id_algoritmo(self):
        del self.__idAlgoritmo


    def del_nombre(self):
        del self.__nombre


    def del_descripcion(self):
        del self.__descripcion


    def del_precondiciones(self):
        del self.__precondiciones


    def del_inputs(self):
        del self.__inputs


    def del_targets(self):
        del self.__targets


    def del_poscondiciones(self):
        del self.__poscondiciones

    idAlgoritmo = property(get_id_algoritmo, set_id_algoritmo, del_id_algoritmo, "idAlgoritmo's docstring")
    nombre = property(get_nombre, set_nombre, del_nombre, "nombre's docstring")
    descripcion = property(get_descripcion, set_descripcion, del_descripcion, "descripcion's docstring")
    precondiciones = property(get_precondiciones, set_precondiciones, del_precondiciones, "precondiciones's docstring")
    inputs = property(get_inputs, set_inputs, del_inputs, "inputs's docstring")
    targets = property(get_targets, set_targets, del_targets, "targets's docstring")
    poscondiciones = property(get_poscondiciones, set_poscondiciones, del_poscondiciones, "poscondiciones's docstring")
            
    