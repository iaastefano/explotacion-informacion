
class Proyecto:
    def __init__(self, idProyecto, nombre, descripcion, lstDatasets, activo, fecha_creacion, fecha_ult_mod, usuario):
        self._idProyecto = idProyecto
        self._nombre = nombre
        self._descripcion = descripcion
        self._lstDatasets = lstDatasets
        self._activo = activo
        self._fecha_creacion = fecha_creacion
        self._fecha_ult_mod = fecha_ult_mod
        self._usuario = usuario

    def get_id_proyecto(self):
        return self.__idProyecto


    def get_nombre(self):
        return self.__nombre


    def get_descripcion(self):
        return self.__descripcion


    def get_lst_datasets(self):
        return self.__lstDatasets


    def get_activo(self):
        return self.__activo


    def get_fecha_creacion(self):
        return self.__fecha_creacion


    def get_fecha_ult_mod(self):
        return self.__fecha_ult_mod


    def get_usuario(self):
        return self.__usuario


    def set_id_proyecto(self, value):
        self.__idProyecto = value


    def set_nombre(self, value):
        self.__nombre = value


    def set_descripcion(self, value):
        self.__descripcion = value


    def set_lst_datasets(self, value):
        self.__lstDatasets = value


    def set_activo(self, value):
        self.__activo = value


    def set_fecha_creacion(self, value):
        self.__fecha_creacion = value


    def set_fecha_ult_mod(self, value):
        self.__fecha_ult_mod = value


    def set_usuario(self, value):
        self.__usuario = value


    def del_id_proyecto(self):
        del self.__idProyecto


    def del_nombre(self):
        del self.__nombre


    def del_descripcion(self):
        del self.__descripcion


    def del_lst_datasets(self):
        del self.__lstDatasets


    def del_activo(self):
        del self.__activo


    def del_fecha_creacion(self):
        del self.__fecha_creacion


    def del_fecha_ult_mod(self):
        del self.__fecha_ult_mod


    def del_usuario(self):
        del self.__usuario

    idProyecto = property(get_id_proyecto, set_id_proyecto, del_id_proyecto, "idProyecto's docstring")
    nombre = property(get_nombre, set_nombre, del_nombre, "nombre's docstring")
    descripcion = property(get_descripcion, set_descripcion, del_descripcion, "descripcion's docstring")
    lstDatasets = property(get_lst_datasets, set_lst_datasets, del_lst_datasets, "lstDatasets's docstring")
    activo = property(get_activo, set_activo, del_activo, "activo's docstring")
    fecha_creacion = property(get_fecha_creacion, set_fecha_creacion, del_fecha_creacion, "fecha_creacion's docstring")
    fecha_ult_mod = property(get_fecha_ult_mod, set_fecha_ult_mod, del_fecha_ult_mod, "fecha_ult_mod's docstring")
    usuario = property(get_usuario, set_usuario, del_usuario, "usuario's docstring")
    
    