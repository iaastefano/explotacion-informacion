# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 23:58:16 2018

@author: intel 2015
"""
import os

from flask import Flask, session, flash, jsonify, request, render_template, redirect

from helpers import file_helper
from helpers.respuesta import Respuesta
from services import usuario_service, proyecto_service, dataset_service, algoritmo_service, variable_service, \
    test_service, regla_service

app = Flask(__name__)



@app.errorhandler(404)
def page_not_found(error):
    return render_template('share/page_not_found.html'), 404


@app.route('/')
def redirect_to_login():
    return redirect('/login')


@app.route('/login', methods=['POST', 'GET'])
def login():
    result = None
    if request.method == 'POST':
        try:
                email = request.form['email']
                contrasenia = request.form['contrasenia']
                respuesta, usuario = usuario_service.autenticar(email, contrasenia)
                if respuesta.codigo == 200:
                    session['logged_in'] = True
                    session['_id_usuario'] = str(usuario._id)
                    session['nombre_completo'] = usuario.nombre + " " + usuario.apellido
                    session['admin'] = usuario.admin
                    flash(respuesta.mensaje, respuesta.categoria)
                    result = redirect('/proyecto/index')
                else:
                    session['logged_in'] = False
                    flash(respuesta.mensaje, respuesta.categoria)
                    result = render_template('login/login.html', email=email)
        except:
            flash('Ha ocurrido un error al intentar loguearse. Por favor intentelo de nuevo.', 'error')
            result = render_template('login/login.html')
    else:
        if 'logged_in' in session and session['logged_in']:
            if 'en_proyecto' in session and '_id_proyecto' in session:
                result = redirect('/proyecto/ver/' + session.get('_id_proyecto'))
            else:
                result = redirect('/proyecto/index')
        else:
            result = render_template('login/login.html')
    return result


@app.route('/signin', methods=['POST', 'GET'])
def signin():
    if request.method == 'POST':
        try:
                nombre = request.form['nombre']
                apellido = request.form['apellido']
                email = request.form['email']
                contrasenia = request.form['contrasenia']
                respuesta = usuario_service.registrar(nombre, apellido, email, contrasenia)
                if respuesta.codigo == 200:
                    flash(respuesta.mensaje, respuesta.categoria)
                    result = redirect('/login')
                else:
                    flash(respuesta.mensaje, respuesta.categoria)
                    result = render_template('login/signin.html', nombre=nombre, apellido=apellido, email=email)
        except NameError:
            flash("Ha ocurrido un error al registrarse. Por favor vuelva a intentarlo.")
            result = render_template('login/signin.html')
    else:
        result = render_template('login/signin.html')
    return result


@app.route('/logout', methods=['POST', 'GET'])
def logout():
    try:
        session.pop('logged_in', None)
        session.pop('_id_usuario', None)
        session.pop('nombre_completo', None)
        flash('Has salido.', 'info')
    except:
        return redirect('/login')
    return redirect('/login')


@app.route('/admin/index', methods=['POST', 'GET'])
def admin():
    try:
        usuarios = usuario_service.traer_usuarios()
        result = render_template('admin/index.html', usuarios=usuarios)
    except:
        flash('Ha ocurrido un error al mostrar los usuario. Por favor recargue la página.','danger')
        result = render_template('admin/index.html')
    return result


@app.route('/_autorizar_usuario')
def _autorizar_usuario():
    try:
        _id_usuario = request.args.get('_id_usuario', '', type=str)
        _autorizado = request.args.get('autorizado', '', type=str)
        autorizado:bool = _autorizado == "True"
        if autorizado == '':
            flash('Ha ocurrido un error al autorizar/desautorizar el usuario el usuario', 'danger')
            return jsonify(result='Ha ocurrido un error al autorizar/desautorizar el usuario.', codigo=100, category='danger')
        respuesta = usuario_service.autorizar_usuario(_id_usuario, autorizado)
        flash(respuesta.mensaje, respuesta.categoria)
        return jsonify(result = respuesta.mensaje, codigo=respuesta.codigo, category= respuesta.categoria)
    except NameError as error:
        flash('Ha ocurrido un error al autorizar/desautorizar el usuario el usuario', 'danger')
        return jsonify(result = 'Ha ocurrido un error al autorizar/desautorizar el usuario.', codigo=100, category='danger')


# FIN LOGIN CONTROLLER


# PROYECTO CONTROLLER

@app.route('/proyecto/index', methods=['POST', 'GET'])
def index_proyecto():
    proyectos = None
    try:
        _id_usuario = session.get('_id_usuario')
        proyectos = proyecto_service.traer_proyectos(_id_usuario)
        result = render_template('proyecto/index.html', proyectos=proyectos)
    except NameError as error:
        flash('Ha ocurrido un error al cargar los proyectos.', 'danger')
        result = render_template('proyecto/index.html')
    return result


@app.route('/proyecto/crear', methods=['POST', 'GET'])
def crear_proyecto():
    if request.method == 'POST':
        try:
            nombre = request.form['nombre_proyecto']
            descripcion = request.form['descripcion_proyecto']
            _id_usuario = session.get('_id_usuario')
            respuesta, _id_proyecto_creado = proyecto_service.iniciar_proyecto(nombre, descripcion, _id_usuario)
            flash(respuesta.mensaje, respuesta.categoria)
            if respuesta.codigo == 200:
                result = redirect('/proyecto/' + _id_proyecto_creado)
            else:
                result = redirect('/proyecto/index')
        except:
            flash('Ha ocurrido un error al crear el proyecto.', 'danger')
            result = redirect('/proyecto/crear')
        return result
    else:
        try:
            return render_template('proyecto/crear.html')
        except NameError as error:
            flash('Ha ocurrido un error al cargar la página para crear un proyecto.', 'danger')
            return redirect('/proyecto/index')


@app.route('/proyecto/editar', methods=['POST', 'GET'])
def editar_proyecto():
    if request.method == 'POST':
        try:
            nombre = request.form['nombre_proyecto']
            descripcion = request.form['descripcion_proyecto']
            _id_proyecto = request.form['_id_proyecto']
            _id_usuario = session.get('_id_usuario')
            respuesta = proyecto_service.editar_proyecto(nombre, descripcion, _id_usuario, _id_proyecto)
            flash(respuesta.mensaje, respuesta.categoria)
            result = redirect('/proyecto/index')
        except:
            flash('Ha ocurrido un error al editar el proyecto.', 'danger')
            result = redirect('/proyecto/index')
        return result


@app.route('/proyecto/eliminar', methods=['POST', 'GET'])
def eliminar_proyecto():
    if request.method == 'POST':
        try:
            _id_proyecto = request.form['_id_proyecto']
            respuesta = proyecto_service.eliminar_proyecto(_id_proyecto)
            flash(respuesta.mensaje, respuesta.categoria)
            result = redirect('/proyecto/index')
        except:
            flash('Ha ocurrido un error al eliminar el proyecto.', 'danger')
            result = redirect('/proyecto/index')
        return result


@app.route('/proyecto/<_id_proyecto>', methods=['POST', 'GET'])
def abrir_proyecto(_id_proyecto):
    try:
        session['en_proyecto'] = True
        session['_id_proyecto'] = _id_proyecto
        return redirect('/proyecto/' + _id_proyecto + '/dataset/index')
    except NameError:
        print(NameError)
        flash('Ha ocurrido un error al abrir el proyecto.', 'danger')
        result = redirect('/proyecto/index')
    return result


@app.route('/proyecto/<_id_proyecto>/dataset/index', methods=['POST', 'GET'])
def index_dataset(_id_proyecto):
    datasets = None
    try:
        proyecto = proyecto_service.traer_proyecto_por_id(_id_proyecto)
        datasets = dataset_service.traer_datasets(_id_proyecto)
        #_id_usuario = session.get('_id_usuario')
        session['nombre_proyecto'] = proyecto.nombre
        result = render_template('dataset/index.html', datasets=datasets)
    except NameError as error:
        flash('Ha ocurrido un error al cargar los datasets.', 'danger')
        result = render_template('dataset/index.html')
    return result


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>', methods=['POST', 'GET'])
def abrir_dataset(_id_proyecto, _id_dataset):
    try:
        respuesta, dataset = dataset_service.traer_dataset_por_id(_id_dataset)
        if respuesta.codigo == 200:
            session['en_dataset'] = True
            session['_id_dataset'] = _id_dataset
            session['nombre_dataset'] = dataset.nombre
            result = redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/' + dataset.siguiente_paso)
        else:
            flash(respuesta.mensaje, respuesta.categoria)
            result = redirect('/proyecto/ver/' + _id_proyecto)
    except NameError:
        flash('Ha ocurrido un error al abrir el dataset.', 'danger')
        result = redirect('/proyecto/ver/' + _id_proyecto)
    return result


@app.route('/proyecto/<_id_proyecto>/dataset/importar', methods=['POST', 'GET'])
def dataset_importar(_id_proyecto):
    result = None
    respuesta = None
    if request.method == 'POST':
        try:
            nombre = request.form['nombre_dataset']
            descripcion = request.form['descripcion_dataset']
            file = request.files['file_dataset']
            respuesta, _id_dataset_creado = dataset_service.importar_dataset(nombre, descripcion, _id_proyecto)
            if respuesta.codigo == 200:
                respuesta, variables = file_helper.importar_variables(file)
                if respuesta.codigo == 200:
                    respuesta = variable_service.agregar_variables(_id_dataset_creado, variables)
                    if respuesta.codigo == 200:
                        flash(respuesta.mensaje, respuesta.categoria)
                        return redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset_creado)
            flash(respuesta.mensaje, respuesta.categoria)
            return redirect('/proyecto/' + _id_proyecto + '/dataset/importar')
        except Exception as e:
                print(str(e))
                mensaje = 'Ha ocurrido un error al iniciar el dataset.'
                categoria = 'danger'
                flash(mensaje, categoria)
                return redirect('/proyecto/' + _id_proyecto + '/dataset/importar')
    else:
        try:
            proyecto = proyecto_service.traer_proyecto_por_id(_id_proyecto)
            result = render_template('dataset/importar.html', proyecto=proyecto)
        except Exception as e:
            pass
        return result


@app.route('/proyecto/<_id_proyecto>/dataset/iniciar', methods=['POST', 'GET'])
def dataset_iniciar(_id_proyecto):
    result = None
    if request.method == 'POST':
        try:
            nombre = request.form['nombre_dataset']
            descripcion = request.form['descripcion_dataset']
            generado = True
            respuesta, _id_dataset_creado = dataset_service.iniciar_dataset(nombre, descripcion, _id_proyecto)
            if respuesta.codigo == 200:
                flash(respuesta.mensaje, respuesta.categoria)
                return redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset_creado)
            else:
                flash(respuesta.mensaje, respuesta.categoria)
                result = redirect('/proyecto/' + _id_proyecto + '/dataset/iniciar')
        except:
                flash('Ha ocurrido un error al iniciar el dataset.', 'danger')
                result = redirect('/proyecto/' + _id_proyecto + '/dataset/iniciar')
        return result
    else:
        proyecto = proyecto_service.traer_proyecto_por_id(_id_proyecto)
        paso = 'iniciar_dataset'
        result = render_template('dataset/iniciar.html', proyecto=proyecto, paso=paso)
        return result


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>/editar', methods=['POST', 'GET'])
def dataset_editar(_id_proyecto, _id_dataset):
    result = None
    if request.method == 'POST':
        try:
            nombre = request.form['nombre_dataset']
            descripcion = request.form['descripcion_dataset']
            respuesta = dataset_service.editar_dataset(_id_proyecto, _id_dataset, nombre, descripcion)
            if respuesta.codigo == 200:
                flash(respuesta.mensaje, respuesta.categoria)
                result = redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/definir_variables')
            else:
                flash(respuesta.mensaje, respuesta.categoria)
                result = redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/editar')
        except:
                flash('Ha ocurrido un error al editar el dataset.', 'danger')
                result = redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/editar')
        return result
    else:
        respuesta, dataset = dataset_service.traer_dataset_por_id(_id_dataset)
        flash(respuesta.mensaje, respuesta.categoria)
        result = render_template('dataset/iniciar.html',
                                 nombre_dataset=dataset.nombre, descripcion_dataset=dataset.descripcion)
        return result


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>/definir_reglas', methods=['POST', 'GET'])
def dataset_definir_reglas(_id_proyecto, _id_dataset):
    proyecto = proyecto_service.traer_proyecto_por_id(_id_proyecto)
    reglas = regla_service.traer_reglas(_id_dataset)
    variables = variable_service.traer_variables(_id_dataset)
    result = render_template('dataset/definir_reglas.html', proyecto=proyecto, reglas=reglas, variables=variables)
    return result


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>/agregar_regla', methods=['POST', 'GET'])
def dataset_agregar_regla(_id_proyecto, _id_dataset):
    nombre = request.form['nombre']
    cantidad = request.form['cantidad']
    porcentaje = request.form['porcentaje']
    variables = variable_service.traer_variables(_id_dataset)
    response = regla_service.agregar_regla(_id_dataset, nombre, cantidad, porcentaje)
    flash(response.mensaje, response.categoria)
    return redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/definir_reglas')


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>/eliminar_regla/<_id_regla>', methods=['POST', 'GET'])
def dataset_eliminar_regla(_id_proyecto, _id_dataset, _id_regla):
    response = regla_service.eliminar_regla(_id_regla)
    flash(response.mensaje, response.categoria)
    return redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/definir_reglas')


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>/definir_variables', methods=['POST', 'GET'])
def dataset_definir_variables(_id_proyecto, _id_dataset):
    proyecto = proyecto_service.traer_proyecto_por_id(_id_proyecto)
    variables = variable_service.traer_variables(_id_dataset)
    respuesta, dataset = dataset_service.traer_dataset_por_id(_id_dataset)
    result = render_template('dataset/definir_variables.html', proyecto=proyecto, variables=variables, generado=dataset.generado)
    return result


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>/agregar_variable', methods=['POST', 'GET'])
def dataset_agregar_variable(_id_proyecto, _id_dataset):
    nombre = request.form['nombre']
    tipo = request.form['tipo']
    dominio = request.form['dominio']
    try:
        null = request.form['null'] == "on"
    except:
        null = False
    response, _id_variable = variable_service.agregar_variable(_id_dataset, nombre, tipo, dominio, null)
    flash(response.mensaje, response.categoria)
    return redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/definir_variables')


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>/eliminar_variable/<_id_variable>', methods=['POST', 'GET'])
def dataset_eliminar_variable(_id_proyecto, _id_dataset, _id_variable):
    response = variable_service.eliminar_variable(_id_variable)
    flash(response.mensaje, response.categoria)
    return redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/definir_variables')
# FIN DATASET CONTROLLER


@app.route('/proyecto/<_id_proyecto>/dataset/<_id_dataset>/editar_variable/<_id_variable>', methods=['POST', 'GET'])
def dataset_editar_variable(_id_proyecto, _id_dataset, _id_variable):
    nombre = request.form['nombre']
    tipo = request.form['tipo']
    dominio = request.form['dominio']
    try:
        null = request.form['null'] == "on"
    except:
        null = False
    response = variable_service.editar_variable(_id_dataset, _id_variable, nombre, tipo, dominio, null)
    flash(response.mensaje, response.categoria)
    return redirect('/proyecto/' + _id_proyecto + '/dataset/' + _id_dataset + '/definir_variables')
# FIN DATASET CONTROLLER


# ALGORITMOS CONTROLLER
@app.route('/_traer_algoritmo')
def _traer_algoritmo():
    try:
        codigo = request.args.get('codigo', '', type=str)
        algoritmo = algoritmo_service.traer_algoritmo(codigo)
        return jsonify(algoritmo_nombre = algoritmo.nombre, algoritmo_descripcion = algoritmo.descripcion, codigo=200)
    except NameError as error:
        return jsonify(result = 'Ha ocurrido un error al traer el algoritmo.', codigo=100, category='danger')

# FIN ALGORITMOS CONTROLLER

# FIN LOGICA DE CONTROLADORES

# TESTS

@app.route('/test/importar', methods=['POST', 'GET'])
def importar():
    result = None
    if request.method == 'POST':
        pass
        try:
            nombre = request.form['nombre_dataset']
            descripcion = request.form['descripcion_dataset']
            file = request.files['file_dataset']
            respuesta, _id_dataset_creado = test_service.importar_dataset(nombre, descripcion, file)
        except Exception as e:
                print(str(e))
                flash(str(e), 'danger')
        return result
    else:
        result = render_template('test/importar.html')
        return result

@app.route('/test/mensajes', methods=['POST', 'GET'])
def mensajes():
    flash('Este es un mensaje en verde', 'success')
    return render_template('/share/_mensajes.html')


@app.route('/test/reglas', methods=['POST', 'GET'])
def reglas():
    session['_id_dataset'] = "5c3e1888906668200830ef04"
    _id_dataset = '5c3e1888906668200830ef04'
    if request.method == 'GET':
        variables = variable_service.traer_variables(_id_dataset)
        json_variables = variable_service.traer_variables_json(_id_dataset)
        return render_template('/test/regla.html', variables=variables, json_variables=json_variables)
    else:
        return redirect('/test/reglas')


@app.route('/_traer_variables')
def _traer_variables():
    try:
        _id_dataset = request.args.get('_id_dataset', '', type=str)
        variables = variable_service.traer_variables_json(_id_dataset)
        return jsonify(variables = variables, codigo=200)
    except NameError as error:
        return jsonify(result = 'Ha ocurrido un error al traer el algoritmo.', codigo=100, category='danger')
# FIN TESTS


if __name__ == "__main__":
    app.secret_key = os.urandom(12)  # TODO: ¿PARA QUE SE USA LA SECRET_KEY?
    app.run(debug=True, host='localhost', port=4000)
