from helpers import db_connection as db
from helpers.respuesta import Respuesta
import datetime
from model import Model
from bson.objectid import ObjectId
import pandas as pd
from pandas import read_excel

def iniciar_dataset(nombre: str, descripcion: str, _id_proyecto):
    _id_dataset = None
    try:
        if not existe_dataset(_id_proyecto, nombre):
            _id_dataset = str(db.datasets.insert_one({'nombre': nombre, 'descripcion': descripcion,
                                                    '_id_proyecto': ObjectId(_id_proyecto), 'fecha_creacion': datetime.datetime.now(), 'siguiente_paso': 'definir_variables'}).inserted_id)
            result = Respuesta("succees", 200, "Dataset iniciado exitosamente!")
        else:
            result = Respuesta("warn", 300, "Ya existe un dataset con este nombre")
    except NameError as error:
        result = Respuesta("danger", 100, "Hubo un error al crear el dataset.")
    return result, _id_dataset


def traer_datasets(_id_proyecto):
    datasets = []
    try:
        for dataset in db.datasets.find({'_id_proyecto': ObjectId(_id_proyecto)}):
            datasets.append(Model(dataset))
    except NameError:
        print(str(NameError))
    return datasets


def traer_dataset_por_id(_id_dataset):
    try:
        dataset = Model(db.datasets.find_one({'_id': ObjectId(_id_dataset)}))
        result = Respuesta('success', 200, 'Dataset traido exitosamente!')
    except:
        result = Respuesta('error', 100, 'Ha ocurrido un error al traer el dataset')
        dataset = None
    return result, dataset


def traer_dataset(_id_proyecto: str, nombre: str):
    try:
        dataset = Model(db.datasets.find_one({'_id_proyecto': ObjectId(_id_proyecto), 'nombre': nombre}))
        result = Respuesta("succees", 200, "Dataset iniciado exitosamente!")
    except:
        result = Respuesta("warn", 100, "No se ha encontrado el dataset")
        dataset = None
    return result, dataset


def existe_dataset(_id_proyecto: str, nombre: str):
    try:
        existe = db.datasets.find({'_id_proyecto': ObjectId(_id_proyecto), 'nombre': nombre}).count() > 0
    except:
        existe = False
    return existe


def importar_dataset(nombre: str, descripcion: str, _id_proyecto, file):
    try:
        file_read = pd.read_excel(file)
        variables = definir_variables_importadas(file_read)
    except Exception as e:
        print(str(e))


def definir_variables_importadas(file_read):
    # variables = []
    for column_name in file_read.columns:
        column = file_read[column_name]
        definir_variable_importada(column)
    # return variables


def definir_variable_importada(column):

    for i in column.index:
        print(column[i])


def agregar_variable(nombre, tipo_desc, dominio, valores):
    try:
        db.variables.insert_one({'nombre': nombre, 'tipo': tipo_desc, 'dominio': dominio})
    except Exception as error:
        print('ERROR: ' + error)

