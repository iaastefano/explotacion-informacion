from helpers import db_connection as db
from helpers.respuesta import Respuesta
import datetime
from model import Model
from bson.objectid import ObjectId


def iniciar_dataset(nombre: str, descripcion: str, _id_proyecto):
    _id_dataset = None
    try:
        if not existe_dataset(_id_proyecto, nombre):
            _id_dataset = str(db.datasets.insert_one({'nombre': nombre, 'descripcion': descripcion,
                                                      '_id_proyecto': ObjectId(_id_proyecto),
                                                      'fecha_creacion': datetime.datetime.now(),
                                                      'siguiente_paso': 'definir_variables', 'generado': True,
                                                      'iniciado': True}).inserted_id)
            result = Respuesta("success", 200, "Dataset iniciado exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe un dataset con este nombre")
    except NameError as error:
        result = Respuesta("danger", 100, "Hubo un error al crear el dataset.")
    return result, _id_dataset


def editar_dataset(_id_proyecto: str, _id_dataset: str, nombre: str, descripcion: str):
    try:
        if not existe_dataset(_id_proyecto, nombre):
            db.datasets.update_one({'_id_dataset': _id_dataset}, {'$set': {'nombre': nombre, 'descripcion': descripcion}})
            result = Respuesta("success", 200, "Dataset editado exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe un dataset con este nombre")
    except NameError as error:
        result = Respuesta("danger", 100, "Hubo un error al editar el dataset.")
    return result


def traer_datasets(_id_proyecto):
    datasets = []
    try:
        for dataset in db.datasets.find({'_id_proyecto': ObjectId(_id_proyecto)}):
            datasets.append(Model(dataset))
    except NameError:
        print(str(NameError))
    return datasets


def traer_dataset_por_id(_id_dataset):
    try:
        dataset = Model(db.datasets.find_one({'_id': ObjectId(_id_dataset)}))
        result = Respuesta('success', 200, 'Dataset traido exitosamente!')
    except Exception as error:
        result = Respuesta('danger', 100, 'Ha ocurrido un error al traer el dataset')
        dataset = None
    return result, dataset


def traer_dataset(_id_proyecto: str, nombre: str):
    try:
        dataset = Model(db.datasets.find_one({'_id_proyecto': ObjectId(_id_proyecto), 'nombre': nombre}))
        result = Respuesta("succees", 200, "Dataset iniciado exitosamente!")
    except:
        result = Respuesta("warn", 100, "No se ha encontrado el dataset")
        dataset = None
    return result, dataset


def existe_dataset(_id_proyecto: str, nombre: str):
    try:
        existe = db.datasets.find({'_id_proyecto': ObjectId(_id_proyecto), 'nombre': nombre}).count() > 0
    except:
        existe = False
    return existe


def importar_dataset(nombre: str, descripcion: str, _id_proyecto):
    _id_dataset = None
    result = None
    try:
        if not existe_dataset(_id_proyecto, nombre):
            _id_dataset = str(db.datasets.insert_one({'nombre': nombre, 'descripcion': descripcion,
                                                      '_id_proyecto': ObjectId(_id_proyecto),
                                                      'fecha_creacion': datetime.datetime.now(),
                                                      'siguiente_paso': 'definir_variables', 'generado': False,
                                                      'iniciado': True}).inserted_id)
            result = Respuesta("success", 200, "Dataset iniciado exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe un dataset con este nombre")
    except NameError as error:
        result = Respuesta("danger", 100, "Hubo un error al crear el dataset.")
    return result, _id_dataset

