from bson import ObjectId

from helpers import db_connection as db
from model import Model


def traer_algoritmos_por_clasificacion(clasificacion):
    algoritmos = []
    try:
        for algoritmo in db.algoritmos.find({'clasificacion': clasificacion}):
            algoritmos.append(Model(algoritmo))
    except:
        pass
    return algoritmos


def traer_clasificaciones():
    clasificaciones = []
    try:
        for clasificacion in db.clasificaciones.find():
            clasificaciones.append(Model(clasificacion))
    except:
        pass
    return clasificaciones


def traer_algoritmo(codigo):
    algoritmo = None
    try:
        algoritmo_db = db.algoritmos.find_one({'codigo': codigo})
        algoritmo = Model(algoritmo_db)
    except:
        raise Exception('No se ha podido encontrar el algoritmo.')
    return algoritmo