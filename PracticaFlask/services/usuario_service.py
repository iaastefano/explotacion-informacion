from bson import ObjectId

from helpers import db_connection as db
from helpers.respuesta import Respuesta
import datetime
from model import Model
import json


def autenticar(email: str, contrasenia: str):
    usuario = None
    try:
        usuario_db = db.usuarios.find_one({'email': email, 'contrasenia': contrasenia})
        if usuario_db is not None:
            usuario = Model(usuario_db)
            if usuario.autorizado:
                result = Respuesta("success", 200, "Bienvenido al sistema!")
            else:
                result = Respuesta("warning", 301, "El usuario no está autorizado para ingresar.")
        else:
            result = Respuesta("warning", 300, "Usuario o contrasenia incorrecto.")
    except NameError as error:
        result = Respuesta("danger", 100, "No se pudo autenticar el usuario.")
    return result, usuario


def registrar(nombre: str, apellido: str, mail: str, contrasenia: str):
    try:
        usuarios = db.usuarios.find({'email': mail})
        if usuarios.count() > 0:
            result = Respuesta("warning", 300, "Ya existe un usuario con este email.")
        else:
            db.usuarios.insert_one({'email': mail, 'contrasenia': contrasenia, 'nombre': nombre, 'apellido': apellido,
                                    'fecha_ingreso': datetime.datetime.now()})
            result = Respuesta("success", 200, "Registro exitoso!")
    except NameError as error:
        result = Respuesta("danger", 100, "Ha ocurrido un error al registrar el usuario.")
    return result


def traer_usuario_por_id(_id: str):
    usuario = None
    try:
        usuario_db = db.usuarios.find_one({'_id': ObjectId(_id)})
        usuario = Model(usuario_db)
    except:
        raise Exception('No se ha podido encontrar el usuario.')
    return usuario


def traer_usuario(email: str):
    usuario = None
    try:
        usuario_db = db.usuarios.find_one({'email': email})
        usuario = Model(usuario_db)
    except:
        raise Exception('No se ha podido encontrar el usuario.')
    return usuario


def traer_usuarios():
    usuarios = []
    try:
        for usuario in db.usuarios.find():
            usuarios.append(Model(usuario))
    except:
        raise Exception('No se ha encontrado ningun usuario.')
    return usuarios


def modificar_usuario(usuario):
    try:
        db.usuarios.update({'_id': usuario._id}, usuario.__dict__)
    except:
        raise Exception('No se ha podido actualizar el usuario')


def autorizar_usuario(_id: str, autorizado: bool):
    try:
        usuario = traer_usuario_por_id(_id)
        if usuario.autorizado == autorizado:
            if usuario.autorizado:
                mensaje = "El usuario ya estaba autorizado anteriormente."
            else:
                mensaje = "El usuario ya no estaba autorizado anteriormente."
            result = Respuesta("warning", 300, mensaje)
        else:
            usuario.autorizado = autorizado
            if usuario.autorizado:
                mensaje = "El usuario ha sido autorizado con exito!"
            else:
                mensaje = "El usuario ha sido desautorizado con exito!"
            modificar_usuario(usuario)
            result = Respuesta("success", 200, mensaje)
    except NameError as error:
        if autorizado:
            mensaje = "Ha ocurrido un error al autorizar al usuario."
        else:
            mensaje = "Ha ocurrido un error al desautorizar al usuario."
        result = Respuesta("danger", 100, mensaje)
    return result




