from helpers import db_connection as db
from helpers.respuesta import Respuesta
import datetime
from model import Model
from bson.objectid import ObjectId


def iniciar_proyecto(nombre: str, descripcion: str, _id_usuario: str):
    _id_proyecto = None
    try:
        if not existe_proyecto(_id_usuario, nombre):
            _id_proyecto = str(db.proyectos.insert_one({'nombre': nombre, 'descripcion': descripcion,
                                                    '_id_usuario': ObjectId(_id_usuario), 'fecha_creacion': datetime.datetime.now()}).inserted_id)
            result = Respuesta("success", 200, "Proyecto creado exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe un proyecto con este nombre")
            return result, _id_proyecto
    except NameError as error:
        result = Respuesta("danger", 100, "Hubo un error al crear el proyecto.")
    return result, _id_proyecto


def editar_proyecto(nombre: str, descripcion: str, _id_usuario: str, _id_proyecto: str):
    try:
        if not existe_proyecto(_id_usuario, nombre):
            db.proyectos.update_one({'_id': ObjectId(_id_proyecto)}, {'$set': {'nombre': nombre, 'descripcion': descripcion}})
            result = Respuesta("success", 200, "Proyecto editado exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe un proyecto con este nombre")
    except Exception as error:
        result = Respuesta("danger", 100, "Hubo un error al editar el proyecto.")
    return result


def eliminar_proyecto(_id_proyecto: str):
    try:
        db.proyectos.delete_one({'_id': ObjectId(_id_proyecto)})
        result = Respuesta('success', 200, 'Proyecto eliminado exitosamente!')
    except Exception as error:
        result = Respuesta('danger', 100, 'Hubo un error al eliminar el proyecto.')
    return result


def traer_proyecto_por_id(_id):
    return Model(db.proyectos.find_one({'_id': ObjectId(_id)}))


def traer_proyecto(_id_usuario, nombre):
    return db.proyectos.find_one({'_id_usuario': ObjectId(_id_usuario), 'nombre': nombre})


def traer_proyectos(_id_usuario):
    return db.proyectos.find({'_id_usuario': ObjectId(_id_usuario)})

def existe_proyecto(_id_usuario, nombre):
    try:
        return db.proyectos.find({'_id_usuario': ObjectId(_id_usuario), 'nombre': nombre}).count() > 0
    except:
        return False


