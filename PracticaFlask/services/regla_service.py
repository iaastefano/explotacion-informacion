from helpers import db_connection as db
from model import Model
from bson.objectid import ObjectId
from helpers.respuesta import Respuesta


def agregar_regla(_id_dataset, nombre, cantidad, porcentaje):
    try:
        if not existe_regla(_id_dataset, nombre):
            db.reglas.insert_one({'nombre': nombre, 'cantidad': cantidad,
                                  'porcentaje': porcentaje, '_id_dataset': ObjectId(_id_dataset)})
            result = Respuesta("success", 200, "Regla agregada exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe una regla con este nombre")
            return result
    except Exception as error:
        result = Respuesta("danger", 100, "Hubo un error al agregar la regla.")
    return result


def editar_regla(_id_dataset, _id_regla, nombre, expresiones, cantidad, porcentaje):
    try:
        if not existe_regla(_id_dataset, nombre):
            db.reglas.update_one({'_id': ObjectId('_id_regla')}, {'$set': {'nombre': nombre, 'expresiones': expresiones,
                                 'cantidad': cantidad, 'porcentaje': porcentaje, '_id_dataset': ObjectId(_id_dataset)}})
            result = Respuesta("success", 200, "Regla editada exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe una regla con este nombre")
            return result
    except NameError as error:
        result = Respuesta("danger", 100, "Hubo un error al editar la regla.")
    return result


def eliminar_regla(_id_regla):
    try:
        db.reglas.remove({'_id': ObjectId(_id_regla)})
        result = Respuesta("info", 200, "Regla eliminada exitosamente!")
        return result
    except Exception as error:
        result = Respuesta("danger", 100, "Hubo un error al eliminar la regla.")
    return result


def traer_reglas(_id_dataset):
    reglas = []
    try:
        for regla in db.reglas.find({'_id_dataset': ObjectId(_id_dataset)}):
            reglas.append(Model(regla))
    except Exception:
        print(str(Exception))
    return reglas


def existe_regla(_id_dataset, nombre):
    try:
        existe = db.reglas.find({'_id_dataset': ObjectId(_id_dataset), 'nombre': nombre}).count() > 0
    except Exception:
        existe = False
    return existe


class Error(Exception):
    """Base class for other exceptions"""
    pass


class AddReglaException(Error):
    """Raised when the input value is too small"""
    pass




