from helpers import db_connection as db
from model import Model
from bson.objectid import ObjectId
from helpers.respuesta import Respuesta

def agregar_variable(_id_dataset, nombre, tipo, dominio, nullable):
    _id_variable = None
    result = Respuesta("danger", 100, "Hubo un error al agregar la variable")
    try:
        if not existe_variable(_id_dataset, nombre):
            _id_variable = db.variables.insert_one({'nombre': nombre, 'tipo': tipo,
                                                    '_id_dataset': ObjectId(_id_dataset), 'dominio': dominio,
                                                    'nullable': nullable})
            result = Respuesta("success", 200, "Variable agregada exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe una variable con este nombre")
            return result, _id_variable
    except NameError as error:
        result = Respuesta("danger", 100, "Hubo un error al agregar la variable.")
    return result, _id_variable


def editar_variable(_id_dataset, _id_variable, nombre, tipo, dominio, nullable):
    try:
        if not existe_variable(_id_dataset, nombre):
            db.variables.update_one({'_id': ObjectId(_id_variable)}, {'$set': {'nombre': nombre, 'tipo': tipo,
                                                                               'dominio': dominio,
                                                                               'nullable': nullable}})
            result = Respuesta("success", 200, "Variable agregada exitosamente!")
        else:
            result = Respuesta("warning", 300, "Ya existe una variable con este nombre")
    except Exception as error:
        result = Respuesta("danger", 100, "Hubo un error al agregar la variable.")
    return result


def agregar_variables(_id_dataset, variables):
    _id_variable = None
    try:
        for i, v in enumerate(variables):
            try:
                response, _id_variable = agregar_variable(_id_dataset, v['nombre'], v['tipo_desc'], v['dominio'], v['nullable'])
                if response.codigo != 200:
                    raise Exception
            except Exception as e:
                raise AddVariableException('No se pudo cargar la variable N°' + str(i))
        result = Respuesta("success", 200, "Variables importadas exitosamente!")
    except AddVariableException as error:
        result = Respuesta("danger", 300, str(error))
    except Exception as error:
        result = Respuesta("danger", 300, 'Ha ocurrido un error inesperado al agregar la variable')
    return result


def eliminar_variable(_id_variable):
    try:
        db.variables.remove({'_id': ObjectId(_id_variable)})
        result = Respuesta("info", 200, "Variable eliminada exitosamente!")
        return result
    except NameError as error:
        result = Respuesta("danger", 100, "Hubo un error al eliminar la variable.")
    return result


def traer_variables(_id_dataset):
    variables = []
    try:
        for variable in db.variables.find({'_id_dataset': ObjectId(_id_dataset)}):
            variables.append(Model(variable))
    except NameError:
        print(str(NameError))
    return variables


def traer_variables_json(_id_dataset):
    variables = []
    try:
        for variable in db.variables.find({'_id_dataset': ObjectId(_id_dataset)}, {'_id': 0, '_id_dataset': 0}):
            variables.append(variable)
    except NameError:
        print(str(NameError))
    return variables


def existe_variable(_id_dataset, nombre):
    try:
        existe = db.variables.find({'_id_dataset': ObjectId(_id_dataset), 'nombre': nombre}).count() > 0
    except NameError:
        existe = False
    return existe


class Error(Exception):
    """Base class for other exceptions"""
    pass


class AddVariableException(Error):
    """Raised when the input value is too small"""
    pass




