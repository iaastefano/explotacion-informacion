from pymongo import MongoClient
from pymongo import database
from pymongo import collection
host = 'localhost'
puerto = 27017
db_name = 'explotacion-informacion'
print("conectandose a la base de datos...")
print("host: " + host)
print("puerto: " + str(puerto))
print("base de datos: " + db_name)
client: MongoClient
db: database
usuarios: collection
proyectos: collection
datasets: collection
clasificaciones: collection
algoritmos: collection
variables: collection
reglas: collection
try:
    client = MongoClient(host, puerto)
    db = client[db_name]
    usuarios = db['usuarios']
    proyectos = db['proyectos']
    datasets = db['datasets']
    clasificaciones = db['clasificaciones']
    algoritmos = db['algoritmos']
    variables = db['variables']
    reglas = db['reglas']
    print("conexion exitosa.")
except NameError as e:
    print("ERROR al conectarse a la base de datos --> " + e)

