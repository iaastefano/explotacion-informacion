import pandas
from typing import Dict
from helpers.respuesta import Respuesta


class Error(Exception):
   """Base class for other exceptions"""
   pass

class ImportException(Error):
   """Raised when the import fails"""
   pass

def _get_valores_unicos(_valores):
    return _valores.value_counts(sort=False).index.values


def _get_dominio(valores_unicos, tipo_desc):
    dominio = []
    if tipo_desc != "Texto":
        minimo = None
        maximo = None
        if tipo_desc == "Entero":
            minimo = int(min(valores_unicos))
            maximo = int(max(valores_unicos))
        if tipo_desc == "Fecha":
            minimo = str(min(valores_unicos))
            maximo = str(max(valores_unicos))
        if tipo_desc == "Flotante":
            minimo = float(min(valores_unicos))
            maximo = float(max(valores_unicos))
        dominio.append(minimo)
        dominio.append(maximo)
    else:
        for v in valores_unicos:
            dominio.append(v)
    return dominio

def _get_tipo(_valores):
    _tipo = _valores.dtype
    return _tipo


def importar_variables(file):
    df = pandas.read_excel(file)
    dtypes: Dict[str, str] = {'datetime64[ns]': 'Fecha', 'object': 'Texto', 'int64': 'Entero', 'float64': 'Flotante'}
    variables = []
    try:
        for i in df.columns:
            try:
                nombre = i
                _valores = df[i]
                tipo = _get_tipo(_valores)
                tipo_desc = dtypes[str(tipo)]
                valores = _valores.get_values()
                nullable = False
                valores_unicos = _get_valores_unicos(_valores)
                dominio = _get_dominio(valores_unicos, tipo_desc)
                variable = {'nombre': nombre, 'tipo_desc': tipo_desc, 'dominio': dominio, 'nullable': False}
                variables.append(variable)
            except Exception as e:
                print(str(e))
                raise ImportException('Error al cargar la columna' + i)
        result = Respuesta('success', 200, 'Variables importadas exitosamente!')
    except ImportException as error:
        result = Respuesta('danger', 100, error)
    except ImportException:
        result = Respuesta('danger', 100, 'Error inesperado al importar las variables')
    return result, variables
