class Respuesta(object):
    def __init__(self, categoria, codigo, mensaje):
        self.categoria: str = categoria
        self.codigo: int = codigo
        self.mensaje: str = mensaje

    def to_string(self):
        return self.categoria + " | " + str(self.codigo) + " | " + self.mensaje